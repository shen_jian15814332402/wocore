import { createRouter, createWebHashHistory } from 'vue-router';

export const constantRoutes=[
    {
        path: '/login',
		name: 'login',
		meta: {
			title: '登录'
		},
		component: () => import('@/views/login/index.vue'),
    },
	{
		path:"/",
		redirect: '/home',
		meta: {
			title: '登录'
		},
	},
	{
		path:"/home",
		meta:{
			title: '管理台'
		},
		component: () => import('@/views/layout/main.vue'),
		children: [
			{
				path: '/home',
				name: 'HomeIndex',
				meta: {
					title: '首页',
					topTree: '/home'
				},
				component: () => import('@/views/home/home.vue'),
			}, 
		]
	}
]

const router = createRouter({
	history: createWebHashHistory(),
	routes: constantRoutes,
});


export default router;
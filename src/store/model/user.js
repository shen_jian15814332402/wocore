import { defineStore } from 'pinia';
import configObj from "@/config"; 
import { setCookie, getCookie, delCookie } from '@/utils/base.js';
import AuthApi from '@/api/auth.js';
import {  setStorage, getStorage } from '@/utils/storageData';
let { strongToken, renderMenu, menu} = configObj;
import { handRouterTable, handMenuData } from '@/utils/router';


export const useUserStore = defineStore('main',{
    state:()=>{
        return {
			token: getStorage(strongToken),
			userInfo: getStorage('userInfo'),
			list: {},
			menus: getStorage(menu),
			renderMenus: getStorage(renderMenu),
		};
    },
    getters: {},
    actions:{
		/**
		 * @description 登录
		 * @param {*} token
		 */
		async afterLogin(info) {
			this.userInfo = this.userInfo || {};
			const { data: { token, loginShopUserVo: { appId, isSuper, shopUserId, userName} } } = info;
			this.token = token;
			const { data } = await AuthApi.getRoleList({});
			const { data:{version,shop_name} } = await AuthApi.getUserInfo({});
			let renderMenusList = handMenuData(JSON.parse(JSON.stringify(data)));
			let menusList = handRouterTable(JSON.parse(JSON.stringify(data)));
			setStorage(JSON.stringify(menusList), menu);
			setStorage(JSON.stringify(renderMenusList), renderMenu);
			this.userInfo.version = version;
			this.userInfo.shop_name = shop_name;
			this.userInfo.userName = userName;
			this.userInfo.shopUserId = shopUserId;
			this.userInfo.isSuper = isSuper;
			this.userInfo.AppID = appId;
			this.renderMenus = renderMenusList;
			this.menus = menusList;
			setStorage(JSON.stringify(token), strongToken); 
			setStorage(JSON.stringify(this.userInfo),'userInfo');
		},
        /**
		 * @description 退出登录
		 * @param {*} token
		 */
		afterLogout() {
			sessionStorage.clear();
			// deleteSessionStorage(null);
			// delCookie(strongToken,null);
			this.token = null;
			this.menus = null;
		},
    }
})

export default useUserStore;

